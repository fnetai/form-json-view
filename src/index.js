import React from "react";
import Layout from "@fnet/react-layout-asya";

import ReactJson from "@uiw/react-json-view";

export default (props) => {

  const [json] = React.useState(props.json || {});
  const [displayDataTypes] = React.useState(props.displayDataTypes || false);
  const [collapsed] = React.useState(props.collapsed || 1);

  return (
    <Layout {...props} contentOverflow="hidden" >
      <ReactJson
        value={json}
        style={{ height: "100%", overflow: "auto", fontSize: "1rem" }}
        displayDataTypes={displayDataTypes}
        collapsed={collapsed}
      />
    </Layout>
  );
};