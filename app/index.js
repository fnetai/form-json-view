import React from "react";

import Form from '../src';

export default function App() {
  return (
    <Form
      title="Title is here"
      // description="Description is here"
      controls={[
        { title: "Back" },
        { title: "Next" },
        { title: "..." },
      ]}

      json={{
        "key": "value",
        "key2": "value2",
        "key3": "value3",
      }}
    />
  );
}