# @fnet/form-json-view

@fnet/form-json-view is a straightforward React component designed to display JSON data in a clear and organized manner. It serves as a handy tool for visualizing JSON data, making it easier to read and understand. This component is especially useful when you need to inspect JSON data structures in your applications without much hassle.

## How It Works

The component utilizes the `ReactJson` viewer to present JSON data. When you use this component, it renders an interface that visually breaks down your JSON data into an easily browsable format. It is wrapped within a `Layout` component to ensure the JSON viewer is displayed seamlessly within your application's existing layout structure.

## Key Features

- **JSON Visualization**: Presents JSON data in a readable format with collapsible sections, allowing users to drill down into data structures as needed.
- **Customizable Display**: Offers options to display or hide data types within the JSON structure.
- **Collapsible By Level**: Provides the ability to initially collapse JSON sections to a specified level for improved data overview.

## Conclusion

In essence, @fnet/form-json-view is a modest yet effective tool for developers seeking to present JSON data in a user-friendly manner. It simplifies the process of inspecting and working with JSON data within React applications, adding convenience without unnecessary complexity.